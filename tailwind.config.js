const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        main: {
          50: "#F6E3F6",
          100: "#EDCAEE",
          200: "#DC98DE",
          300: "#CB65CE",
          400: "#B33AB7",
          500: "#822A85",
          600: "#6B236E",
          700: "#551B56",
          800: "#3E143F",
          900: "#270D28"
        },
        gray: {
          50: "#EDEDED",
          100: "#DCDCDC",
          200: "#BBBBBB",
          300: "#9A9A9A",
          400: "#797979",
          500: "#585858",
          600: "#494949",
          700: "#393939",
          800: "#2A2A2A",
          900: "#1B1B1B"
        },
        info: colors.blue,
        success: colors.green,
        error: colors.red,
        warn: colors.yellow
      }
    },
  },
  plugins: [],
}
