import { Injector, NgModule } from '@angular/core';
import { RemoteDialog, RemoteDialogService } from './remote-dialogs/remote-dialogs.service';


@NgModule({
    imports: [

    ],
    exports: [

    ],
    declarations: [

    ],
    providers: [
        RemoteDialogService
    ],
})
export class LLServicesModule {

}


