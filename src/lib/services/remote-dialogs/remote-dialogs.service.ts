import { Component, Injectable, Injector } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { LLConfirmDialogComponent } from 'src/lib/dialogs/confirm/ll-confirm.dialog';

export class RemoteDialogConfig extends MatDialogConfig {

}

export const LLCommonDialogs: any = {
    "Confirm": LLConfirmDialogComponent
}


@Injectable({ providedIn: 'root' })
export class RemoteDialogService {
    constructor(
        private MatDialog: MatDialog
    ) { }

    private dialogs: any = {};
    private configs: any = {};


    registerDialog(remoteName: string, dialogComponent: Component | any, config: RemoteDialogConfig | null = null) {
        this.dialogs[remoteName] = dialogComponent;
        this.configs[remoteName] = config;
    }

    openDialog(remoteName: string, context: any = null) {
        if (context == null) {
            context = this.configs[remoteName].data;
        }

        return this.MatDialog.open(LLCommonDialogs[remoteName], {
            panelClass: "custom-dialog-container",
            maxWidth: '95vw',
            data: context
        });

    }

    openComponent(component: any, context: any = null) {
        return this.MatDialog.open(component, {
            panelClass: "custom-dialog-container",
            maxWidth: '95vw',
            data: context
        });
    }

}

export function RemoteDialog(remoteDialogName: string, config: RemoteDialogConfig | null = null): ClassDecorator {
    return function (target: any) {
        setTimeout(() => {
            RemoteDialogService.prototype.registerDialog(remoteDialogName, target, config);
        });
    };
}

