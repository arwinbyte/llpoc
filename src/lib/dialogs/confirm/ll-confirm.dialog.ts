import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
    templateUrl: 'll-confirm.dialog.html'
})
export class LLConfirmDialogComponent implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<LLConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() { }

    onConfirm() {
        this.dialogRef.close(true);
    }

    onClose() {
        this.dialogRef.close(false);
    }
}