// import { Component, Input } from "@angular/core";
// import { MatDialog, MatDialogModule } from "@angular/material/dialog";
// import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
// import { Meta, Story } from "@storybook/angular";
// import { LLLibModule } from "src/lib/lib.module";
// import { RemoteDialogService } from "src/lib/services/remote-dialogs/remote-dialogs.service";
// import { LLConfirmDialogComponent } from "./ll-confirm.dialog";


// @Component({})
// class LaunchDialogComponent {
//     constructor(
//         private _dialog: MatDialog,
//         private _remoteDialog: RemoteDialogService
//     ) {
//         this.launch();
//     }

//     launch(): void {
//         this._remoteDialog.openComponent(LLConfirmDialogComponent, { data: "Czy potwierdzasz?" })
//     }
// }

// export default {
//     title: 'high-level/ll-confirm-dialog',
//     component: LaunchDialogComponent,
// } as Meta;

// const Template: Story<LaunchDialogComponent> = (args: LaunchDialogComponent) => ({
//     props: args,
//     moduleMetadata: {
//         imports: [
//             MatDialogModule,
//             LLLibModule,
//             BrowserAnimationsModule
//         ]
//     }
// });


// export const Example = Template.bind({});
// Example.args = {

// };

