import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { LLComponentsModule } from '../components/components.module';
import { RemoteDialogService } from '../services/remote-dialogs/remote-dialogs.service';
import { LLServicesModule } from '../services/services.module';
import { LLConfirmDialogComponent } from './confirm/ll-confirm.dialog';


@NgModule({
    imports: [
        CommonModule,
        MatDialogModule,
        LLComponentsModule,
        LLServicesModule,
    ],
    exports: [
        LLConfirmDialogComponent
    ],
    declarations: [
        LLConfirmDialogComponent
    ],
    providers: [
        RemoteDialogService
    ],
})
export class LLDialogsModule { }