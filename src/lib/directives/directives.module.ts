import { NgModule } from '@angular/core';
import { ViewHostDirective } from './view-host/view-host';


@NgModule({
    imports: [

    ],
    exports: [
        ViewHostDirective
    ],
    declarations: [
        ViewHostDirective
    ],
    providers: [

    ],
})
export class LLDirectivesModule { }
