import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
    selector: 'll-expand',
    templateUrl: 'expand.component.html'
})

export class LLTableExpandComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

    @Input()
    expanded: boolean = false;

    @ContentChild('expand')
    public expand: TemplateRef<LLTableExpandComponent>;

}