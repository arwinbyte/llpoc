import { AfterContentInit, AfterViewChecked, AfterViewInit, Component, ContentChild, ContentChildren, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { LLColumnComponent } from './column/column.component';
import { LLTableExpandComponent } from './expand/expand.component';

@Component({
    selector: 'll-table',
    templateUrl: 'table.component.html'
})

export class LLTableComponent implements OnInit, AfterContentInit {
    constructor() { }

    ngOnInit() {

    }

    @Input() data: any[] | null;

    @ContentChildren(LLColumnComponent)
    columnsQuery: QueryList<LLColumnComponent>;
    columns: LLColumnComponent[];

    @ContentChild(LLTableExpandComponent)
    expandRow: LLTableExpandComponent;

    expandedRowIndex: number | undefined;

    public isInit: boolean = false;

    ngAfterContentInit(): void {
        this.columns = this.columnsQuery.toArray();
        this.isInit = true;
    }

    onExpandRow(index: number) {
        this.expandedRowIndex != index ? this.expandedRowIndex = index : this.expandedRowIndex = undefined;
    }

}

