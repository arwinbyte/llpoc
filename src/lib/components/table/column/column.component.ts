import { AfterViewInit, Component, ContentChild, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
    selector: 'll-column',
    templateUrl: 'column.component.html'
})

export class LLColumnComponent implements OnInit, AfterViewInit {
    constructor() { }

    @Input()
    header: string;

    @Input()
    span: number;

    @Input()
    class: string;

    @Input()
    spanPercent: number;

    get spanPercentCalculated() {
        if (this.span)
            return ((this.span / 12) * 100) + '%'
        else if (this.spanPercent)
            return this.spanPercent + '%';
        else
            return undefined;
    }

    @ContentChild('column')
    public column: TemplateRef<LLColumnComponent>;

    ngOnInit() { }

    ngAfterViewInit(): void {

    }


}