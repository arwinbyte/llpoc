// import { Meta, Story } from "@storybook/angular";
// import { LLLibModule } from "src/lib/lib.module";
// import { UsersModule } from "src/modules/users/user.module";
// import { UsersService } from "src/modules/users/users.service";
// import { LLTableComponent } from "./table.component";

// export default {
//     title: 'medium-level/ll-table',
//     component: LLTableComponent,
//     argTypes: {
//         buttonsSizes: {
//             options: ['small', 'default', 'big'],
//             control: { type: 'radio' }, // Automatically inferred when 'options' is defined
//         },
//         buttonsOrientation: {
//             options: ['horizontal', 'vertical'],
//             control: { type: 'radio' },
//         },
//     }
// } as Meta;


// const Template: Story<LLTableComponent> = (args: LLTableComponent) => ({
//     props: {
//         ...args,
//         testData: UsersService.prototype.fetchUsers()
//     },
//     template: `
//         <ll-table [data]="(testData | async) ?? []">
//             <ll-column [spanPercent]="2" header="Id">
//                 <ng-template #column let-item>
//                     {{item.Id}}
//                 </ng-template>
//             </ll-column>
//             <ll-column [span]="3" header="Name">
//                 <ng-template #column let-item>
//                     {{item.Name}}
//                 </ng-template>
//             </ll-column>
//             <ll-column [span]="2" header="Email">
//                 <ng-template #column let-item>
//                     {{item.Email}}
//                 </ng-template>
//             </ll-column>
//             <ll-column [span]="1" header="Czy potwierdzony">
//                 <ng-template #column let-item>
//                     {{item.IsConfirmed}}
//                 </ng-template>
//             </ll-column>
//             <ll-column [span]="1" header="Akcje">
//                 <ng-template #column let-item>
//                     <ll-buttons-group [sizes]="buttonsSizes" [orientation]="buttonsOrientation">
//                         <ll-button>
//                             Podgląd
//                         </ll-button>
//                         <ll-button color="accent">
//                             Potwierdź
//                         </ll-button>
//                         <ll-button color="error">
//                             Usuń
//                         </ll-button>
//                     </ll-buttons-group>
//                 </ng-template>
//             </ll-column>
//             <ll-expand>
//                 <ng-template #expand let-item>
//                     <div class="p-4 text-gray-500">
//                         {{item.Details}}
//                     </div>
//                 </ng-template>
//             </ll-expand>
//         </ll-table>
//     `,
//     moduleMetadata: {
//         imports: [
//             LLLibModule
//         ]
//     }
// });

// export const Example = Template.bind({})
// Example.args = {
//     buttonsSizes: "default",
//     buttonsOrientation: "vertical"
// } as Partial<LLTableComponent>;
