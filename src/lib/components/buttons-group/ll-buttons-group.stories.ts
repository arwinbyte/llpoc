// import { MatRippleModule } from '@angular/material/core';
// import { Story, Meta } from '@storybook/angular/types-6-0';
// import { LLLibModule } from 'src/lib/lib.module';
// import { LLButtonsGroup } from './ll-buttons-group.component';


// export default {
//     title: 'low-level/ll-buttons-group',
//     component: LLButtonsGroup,
//     argTypes: {
//         buttonsSize: {
//             options: ['small', 'default', 'big'],
//             control: { type: 'radio' }, // Automatically inferred when 'options' is defined
//         }
//     }
// } as Meta;

// const SingleTemplate: Story<LLButtonsGroup> = (args: LLButtonsGroup) => ({
//     props: args,
//     template:
//         `
//         <div class="w-[400px] mx-auto border">
//             <ll-buttons-group [orientation]="orientation" [centered]="centered" >
//                 <ll-button [size]="buttonsSize" color="accent">
//                     Zamknij
//                 </ll-button>
//             </ll-buttons-group>
//         </div>

//     `,
//     moduleMetadata: {
//         imports: [
//             LLLibModule
//         ]
//     }
// });

// const DuoTemplate: Story<LLButtonsGroup> = (args: LLButtonsGroup) => ({
//     props: args,
//     template:
//         `
//         <div class="w-[400px] mx-auto border">
//             <ll-buttons-group [orientation]="orientation" [centered]="centered">
//                 <ll-button [size]="buttonsSize">
//                     Nie
//                 </ll-button>
//                 <ll-button [size]="buttonsSize" color="accent">
//                     Tak
//                 </ll-button>
//             </ll-buttons-group>
//         </div>

//     `,
//     moduleMetadata: {
//         imports: [
//             LLLibModule
//         ]
//     }
// });

// const TrioTemplate: Story<LLButtonsGroup> = (args: LLButtonsGroup) => ({
//     props: args,
//     template:
//         `
//         <div class="w-[400px] h-auto mx-auto border">
//             <ll-buttons-group [orientation]="orientation" [centered]="centered" [fancyTrio]="fancyTrio">
//                 <ll-button [size]="buttonsSize" color="ghost">
//                     Wróć
//                 </ll-button>
//                 <ll-button [size]="buttonsSize">
//                     Zapisz
//                 </ll-button>
//                 <ll-button [size]="buttonsSize" color="accent">
//                     Zapisz i wyślij
//                 </ll-button>
//             </ll-buttons-group>
//         </div>
//     `,
//     moduleMetadata: {
//         imports: [
//             LLLibModule
//         ]
//     }
// });


// export const Single = SingleTemplate.bind({});
// Single.args = {
//     buttonsSize: 'default',
//     fancyTrio: false,
//     orientation: 'horizontal',
//     centered: false,
// } as Partial<LLButtonsGroup>;


// export const Duo = DuoTemplate.bind({});
// Duo.args = {
//     buttonsSize: 'default',
//     fancyTrio: false,
//     orientation: 'horizontal',
//     centered: false,
// } as Partial<LLButtonsGroup>;

// export const Trio = TrioTemplate.bind({});
// Trio.args = {
//     buttonsSize: 'default',
//     fancyTrio: false,
//     orientation: 'horizontal',
//     centered: false,
// } as Partial<LLButtonsGroup>;
