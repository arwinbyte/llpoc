import { AfterViewInit, Component, ContentChildren, Input, OnChanges, OnInit, QueryList, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { LLButtonComponent } from '../button/ll-button.component';

@Component({
    selector: 'll-buttons-group',
    templateUrl: 'll-buttons-group.component.html'
})

export class LLButtonsGroup implements OnInit, AfterViewInit, OnChanges {
    constructor() { }


    ngOnInit() { }

    @Input() centered: boolean = false;

    @Input() orientation: "horizontal" | "vertical" = "horizontal";

    @Input() fancyTrio: boolean = false;

    @Input() sizes: "small" | "default" | "big" = "default";

    @ContentChildren(LLButtonComponent)
    public buttonsQuery: QueryList<LLButtonComponent>;

    buttons: Array<LLButtonComponent> = []

    @ViewChild(TemplateRef)
    content: TemplateRef<any>;

    ngAfterViewInit() {
        this.render();
    }

    render() {
        setTimeout(() => {
            this.buttons = this.buttonsQuery.toArray();

            if (this.orientation == 'vertical') {
                this.buttons.forEach(button => button.stretched = true)
            }
        })
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.render();
    }


}