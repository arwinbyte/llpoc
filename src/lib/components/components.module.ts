import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { LLButtonComponent } from './button/ll-button.component';
import { LLButtonsGroup } from './buttons-group/ll-buttons-group.component';
import { LLContentComponent } from './content/content.component';
import { LLDialogComponent } from './dialog/ll-dialog.component';
import { LLColumnComponent } from './table/column/column.component';
import { LLTableExpandComponent } from './table/expand/expand.component';
import { LLTableComponent } from './table/table.component';
import { LLLabelComponent } from './label/ll-label.component';
import { LLCenterComponent } from './center/ll-center.component';
import { LLIconComponent } from './ll-icon/ll-icon.component';


@NgModule({
    imports: [
        CommonModule,
        MatRippleModule,
        MatTooltipModule
    ],
    exports: [
        LLCenterComponent,
        LLIconComponent,
        LLContentComponent,
        LLButtonComponent,
        LLButtonsGroup,
        LLLabelComponent,
        LLDialogComponent,
        LLTableComponent,
        LLColumnComponent,
        LLTableExpandComponent
    ],
    declarations: [
        LLCenterComponent,
        LLIconComponent,
        LLContentComponent,
        LLButtonComponent,
        LLButtonsGroup,
        LLLabelComponent,
        LLDialogComponent,
        LLTableComponent,
        LLColumnComponent,
        LLTableExpandComponent
    ],
    providers: [

    ],
})
export class LLComponentsModule { }
