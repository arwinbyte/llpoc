// import { CommonModule } from "@angular/common";
// import { Meta, Story } from "@storybook/angular";
// import { LLLibModule } from "src/lib/lib.module";
// import { LLDialogComponent } from "./ll-dialog.component";

// export default {
//     title: 'medium-level/ll-dialog',
//     component: LLDialogComponent,
//     argTypes: {
//         popupSize: {
//             name: "Popup Size",
//             options: ['small', 'medium', 'big', 'large'],
//             control: { type: 'radio' },
//             defaultValue: 'small'
//         },
//         popupTitle: {
//             name: "Popup Title",
//             defaultValue: 'Tytuł popupu',
//             control: { type: 'text' },
//         },
//         titleSize: {
//             name: "titleSize",
//             options: ['text-lg', 'text-xl', 'text-2xl', 'text-3xl'],
//             control: { type: 'radio' },
//             defaultValue: 'text-3xl'
//         },
//         popupContent: {
//             name: "Popup Content",
//             defaultValue: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac lacus orci. Fusce non quam pellentesque, efficitur arcu sit amet, pulvinar diam. Etiam euismod ultricies hendrerit. Fusce porta, ante eu venenatis euismod, neque nulla vulputate neque, non elementum tellus mauris eu nibh. Nam sit amet semper risus, quis aliquam mauris. In ut elit ut ex mattis tincidunt. Donec a elit odio.',
//             control: { type: 'text' },
//         },
//         button1Label: {
//             name: "Button 1 label",
//             control: { type: 'text' },
//             defaultValue: "Wróć"
//         },
//         button1Color: {
//             name: "Button 1 color",
//             options: ['ghost', 'default', 'accent', 'warning', 'error'],
//             control: { type: 'radio' },
//             defaultValue: 'ghost'
//         },
//         button2Label: {
//             name: "Button 2 label",
//             control: { type: 'text' },
//             defaultValue: "Zapisz"
//         },
//         button2Color: {
//             name: "Button 2 color",
//             options: ['ghost', 'default', 'accent', 'warning', 'error'],
//             control: { type: 'radio' },
//             defaultValue: 'default'
//         },
//         button3Label: {
//             name: "Button 3 label",
//             control: { type: 'text' },
//             defaultValue: "Zapisz i wyślij"
//         },
//         button3Color: {
//             name: "Button 3 color",
//             options: ['ghost', 'default', 'accent', 'warning', 'error'],
//             control: { type: 'radio' },
//             defaultValue: 'accent'
//         },
//         buttonsCentered: {
//             name: "Button 3 centered",
//             control: { type: 'boolean' },
//             defaultValue: false
//         }

//     },
// } as Meta;

// const TemplateSingle: Story<LLDialogComponent> = (args: LLDialogComponent) => ({
//     props: args,
//     template:
//         `
//         <div class="flex flex-row justify-center mt-4 ">
//             <div class="inline-block border">
//                 <ll-dialog [size]="popupSize">
//                     <ll-title [font]="titleSize">
//                         {{popupTitle}}
//                     </ll-title>
            
//                     <div ll-content>
//                         {{popupContent}}
//                     </div>

//                     <ll-buttons-group [centered]="buttonsCentered">
//                         <ll-button  [color]="button1Color">
//                             {{button1Label}}
//                         </ll-button>
//                     </ll-buttons-group>
//                 </ll-dialog>
//             </div>
//         </div>

//     `,
//     moduleMetadata:
//     {
//         imports:
//             [
//                 LLLibModule
//             ]
//     }
// });

// const TemplateDuo: Story<LLDialogComponent> = (args: LLDialogComponent) => ({
//     props: args,
//     template:
//         `
//         <div class="flex flex-row justify-center mt-4 ">
//             <div class="inline-block border">
//                 <ll-dialog [size]="popupSize">
//                     <ll-title [font]="titleSize">
//                         {{popupTitle}}
//                     </ll-title>
            
//                     <div ll-content>
//                         {{popupContent}}
//                     </div>

    
//                     <ll-buttons-group [centered]="buttonsCentered">
//                         <ll-button  [color]="button1Color">
//                             {{button1Label}}
//                         </ll-button>
//                         <ll-button [color]="button3Color">
//                             {{button3Label}}
//                         </ll-button>
//                     </ll-buttons-group>
//                 </ll-dialog>
//             </div>
//         </div>

//     `,
//     moduleMetadata:
//     {
//         imports:
//             [
//                 LLLibModule
//             ]
//     }
// });

// const TemplateTrio: Story<LLDialogComponent> = (args: LLDialogComponent) => ({
//     props: args,
//     template:
//         `
//         <div class="flex flex-row justify-center mt-4 ">
//             <div class="inline-block border">
//                 <ll-dialog [size]="popupSize">
//                     <ll-title [font]="titleSize">
//                         {{popupTitle}}
//                     </ll-title>
                    
//                     <div ll-content>
//                         {{popupContent}}
//                     </div>
    
//                     <ll-buttons-group [centered]="buttonsCentered">
//                         <ll-button  [color]="button1Color">
//                             {{button1Label}}
//                         </ll-button>
//                         <ll-button  [color]="button2Color">
//                         {{button2Label}}
//                     </ll-button>
//                         <ll-button [color]="button3Color">
//                             {{button3Label}}
//                         </ll-button>
//                     </ll-buttons-group>
//                 </ll-dialog>
//             </div>
//         </div>

//     `,
//     moduleMetadata:
//     {
//         imports:
//             [
//                 LLLibModule
//             ]
//     }
// });

// export const ExampleSingle = TemplateSingle.bind({});
// ExampleSingle.args = {
//     isInit: true
// } as Partial<LLDialogComponent>;

// export const ExampleDuo = TemplateDuo.bind({});
// ExampleDuo.args = {
//     isInit: true
// } as Partial<LLDialogComponent>;

// export const ExampleTrio = TemplateTrio.bind({});
// ExampleTrio.args = {
//     isInit: true
// } as Partial<LLDialogComponent>;