import { AfterContentChecked, AfterContentInit, AfterViewInit, Component, ContentChild, Input, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { LLButtonsGroup } from '../buttons-group/ll-buttons-group.component';


@Component({
    selector: 'll-dialog',
    templateUrl: 'll-dialog.component.html'
})

export class LLDialogComponent implements OnInit, AfterContentInit {
    constructor(public viewContainerRef: ViewContainerRef) { }

    @Input()
    public size: "small" | "medium" | "big" | "large" = "small";

    @ViewChild(TemplateRef)
    content: TemplateRef<any>;

    @ContentChild(LLButtonsGroup)
    buttonsGroup: LLButtonsGroup

    public isInit: boolean = false;

    ngOnInit() {
        this.viewContainerRef.clear();
    }

    ngAfterContentInit(): void {
        setTimeout(() => {
            this.isInit = true;
            this.buttonsGroup.fancyTrio = true;
        })
    }

}
