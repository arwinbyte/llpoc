
import { Meta, Story } from "@storybook/angular";
import { LLContentComponent } from '../content/content.component';
import { LLLibModule } from "src/lib/lib.module";
import { LABEL_SIZES } from "src/lib/types/sizes.type";
import { LABEL_COLORS } from "src/lib/types/colors.type";

export default {
    title: 'low-level/ll-button',
    component: LLContentComponent,
    argTypes: {
        label: {
            control: {
                type: "text"
            }
        },
        disabled: {
            control: {
                type: "boolean"
            }
        },
        color: {
            control: {
                type: 'radio',
                options: ["main", "default", "ghost", "info", "success", "error", "warn"]
            },
        },
        icon: {
            control: {
                type: 'select',
                options: ["", "image", "edit", "push_pin", "unpin", "download", "refresh", "send", 'content_copy', 'pageview', 'group', 'delete', 'money_off', 'cancel', 'block', 'check', 'add', 'add_circle', 'visibility', 'visibility_off', 'restore_page', 'monitor_heart', 'history', 'sync', 'category', 'publish', "help", "info", "error", "warning", "search"]
            }
        },
        stretched: {
            control: {
                type: 'boolean',
            }
        }
    }
} as Meta;

const Template: Story<LLContentComponent> = (args: LLContentComponent) => ({
    props: args,
    template: `
        <link rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet">
        
        <div class="w-full">
            <ll-button [color]="color" [icon]="icon" [disabled]="disabled" [stretched]="stretched">
                {{label}}
            </ll-button>
        </div>        
    `,
    moduleMetadata: {
        imports: [
            LLLibModule
        ]
    }
});

export const Example = Template.bind({});
Example.args = {
    label: "Click me",
    disabled: false,
    color: "main",
    icon: "add",
    stretched: false
} as Partial<LLContentComponent>;