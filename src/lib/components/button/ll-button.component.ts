import { AfterContentChecked, Component, ContentChild, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ButtonColors, LabelColors } from 'src/lib/types/colors.type';
import { LLLabelComponent } from '../label/ll-label.component';

@Component({
    selector: 'll-button',
    templateUrl: 'll-button.component.html'
})

export class LLButtonComponent implements OnInit, AfterContentChecked {
    constructor(

    ) { }

    ngOnInit() { }

    @Input() color: ButtonColors = "default";
    @Input() icon: string | null = null;
    @Input() blockOnProgress: boolean = true;
    @Input() tooltip: string;
    @Input() stretched: boolean = false;

    @Output() onClick = new EventEmitter();

    @ViewChild("content")
    content: TemplateRef<any>;

    labelColor: LabelColors = "white";

    labelBrighted = false;

    @Input()
    public disabled: boolean = false;

    ngAfterContentChecked(): void {
        this.render();
    }

    render() {
        if (this.color != "ghost" && this.color != "default") {
            this.labelBrighted = true;
            this.labelColor = "white";
        }
        else {
            this.labelBrighted = false;
            this.labelColor = "default";
        }
    }


}