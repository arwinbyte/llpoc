import { CommonModule } from '@angular/common';
import { Injector, ModuleWithProviders, NgModule } from '@angular/core';
import { MatRippleModule } from '@angular/material/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LLComponentsModule } from './components/components.module';
import { LLDialogsModule } from './dialogs/dialogs.module';
import { LLDirectivesModule } from './directives/directives.module';
import { RemoteDialogService } from './services/remote-dialogs/remote-dialogs.service';
import { LLServicesModule } from './services/services.module';


@NgModule({
    imports: [
        LLServicesModule,
        CommonModule,
        LLDialogsModule,
        LLComponentsModule,
        LLDirectivesModule,
    ],
    exports: [
        LLServicesModule,
        LLComponentsModule,
        LLDialogsModule,
        LLDirectivesModule,
    ],
    declarations: [

    ],
    providers: [

    ],
})
export class LLLibModule {
    public static forRoot(): ModuleWithProviders<LLLibModule> {
        return {
            ngModule: LLLibModule,
            providers: [
                RemoteDialogService
            ]
        };
    }

    constructor() {

    }
}

