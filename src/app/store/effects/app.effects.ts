import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { concatMap, map } from 'rxjs';
import { LLConfirmDialogComponent } from 'src/lib/dialogs/confirm/ll-confirm.dialog';
import { RemoteDialogService } from 'src/lib/services/remote-dialogs/remote-dialogs.service';
import * as appActions from './../actions/app.actions';

@Injectable()
export class AppEffects {

  constructor(
    private actions$: Actions,
    private remotes: RemoteDialogService
  ) { }


}
