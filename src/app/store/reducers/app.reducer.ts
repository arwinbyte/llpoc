import { Action, ActionReducer, createAction, createReducer, MetaReducer, on } from '@ngrx/store';
import * as appActions from './../actions/app.actions';

export const AppKey = 'App';

export interface AppState {

}

export const initialState: AppState = {

};

export const reducer = createReducer(
  initialState
);

