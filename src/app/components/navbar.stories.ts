// import { MatIconModule } from "@angular/material/icon";
// import { RouterModule } from "@angular/router";
// import { Meta, Story } from "@storybook/angular";
// import { LLLibModule } from "src/lib/lib.module";
// import { NavBarComponent } from "./navbar.component";

// export default {
//     title: 'medium-level/ll-nav-bar',
//     component: NavBarComponent,
//     argTypes: {
//         // propetyName: {
//         //     options: [true, false],
//         //     control: { type: 'radio' },
//         // },
//     }
// } as Meta;


// const Template: Story<NavBarComponent> = (args: NavBarComponent) => ({
//     props: {
//         ...args,
//     },
//     template: `
//     <link href="https://fonts.googleapis.com/icon?family=Material+Icons&Metarial+Icons+Outlined" rel="stylesheet">
//     <div class="max-w-[48px] h-screen bg-leaselink-500">
//         <ll-buttons-group orientation="vertical">
//             <ll-button (onClick)="router.navigate([''])" size="small" color="ghost">
//                 <mat-icon class="text-white">
//                     <span matTooltip="Menu głowne" matTooltipPosition="right" class="material-icons-outlined">
//                         home
//                     </span>
//                 </mat-icon>
//             </ll-button>

//             <ll-button (onClick)="router.navigate(['users'])" size="small" color="ghost">
//                 <mat-icon matTooltip="Zarządzanie użytkownikami" matTooltipPosition="right" class="text-white">
//                     <span class="material-icons-outlined">
//                         manage_accounts
//                     </span>
//                 </mat-icon>
//             </ll-button>

//             <ll-button class.className="mt-auto" size="small" color="ghost">
//                 <mat-icon class="text-white">
//                     <span matTooltip="Wyloguj" matTooltipPosition="right" class="material-icons-outlined">
//                         logout
//                     </span>
//                 </mat-icon>
//             </ll-button>

//         </ll-buttons-group>
//     </div>
//     `,
//     moduleMetadata: {
//         imports: [
//             LLLibModule,
//             MatIconModule
//         ]
//     }
// });

// export const Example = Template.bind({})
// Example.args = {
// } as Partial<NavBarComponent>;
