import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import * as fromApp from './store/reducers/app.reducer';

import { environment } from 'src/environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './store/effects/app.effects';
import { LLLibModule } from 'src/lib/lib.module';
import { MatIconModule } from '@angular/material/icon';
import { NavBarComponent } from './components/navbar.component';
import { MatDialogModule } from '@angular/material/dialog';
import { LLConfirmDialogComponent } from 'src/lib/dialogs/confirm/ll-confirm.dialog';
import { RouterModule, Routes } from '@angular/router';
import { LLDialogsModule } from 'src/lib/dialogs/dialogs.module';
import { RemoteDialogService } from 'src/lib/services/remote-dialogs/remote-dialogs.service';
import { MatTooltipModule } from '@angular/material/tooltip';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    children: [
      { path: '', component: NavBarComponent, outlet: 'navbar' }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    LLLibModule,
    MatDialogModule,
    MatTooltipModule,
    BrowserModule,
    MatIconModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(fromApp.reducer),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AppEffects])
  ],
  providers: [
    RemoteDialogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {

  }
}
